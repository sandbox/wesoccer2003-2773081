Commerce Mycelium Gear
----------------------

This module provides Mycelium Gear payment option for Drupal Commerce.

For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/wesoccer2003/2773081


** REQUIREMENTS **

Drupal Commerce
  http://www.drupalcommerce.org/
  https://drupal.org/project/commerce
  cURL for PHP 


** MYCELIUM GEAR CONFIGURATION **

You will need an account with Mycelium Gear
https://admin.gear.mycelium.com/users/sign_up
Once you have an account you will need to setup a gateway.
Some of the gateway settings need to be configured for this module.

    - Callback url

        Set the "Callback url" field to:
            http://[yourdomain.com]/mycelium/callback
        For example http://example.com/mycelium/callback

    - After payment redirect to

        Set the "After payment redirect to" field to:
            http://[yourdomain.com]/mycelium/checkout/complete.
        For example http://example.com/mycelium/checkout/complete

    - Auto redirect

        Needs to be checked

    - Active

        Needs to be checked

    - BIP32 pubkey

        You will need to get an Electrum or Mycelium wallet. This is where your
        payments will be deposited to. To get the Electrum BIP32 pubkey go to:
            https://www.youtube.com/watch?v=2E0cwY_M2D4
        For the Mycelium BIP32 pubkey go to:
            https://www.youtube.com/watch?v=-CN_I8vfEL4

    - Pick the wallet that you're using

        Make sure you select the correct wallet.

    - Secret

        When you save your gateway settings for the first time Mycelium will
        provide a secret which needs to be entered into the Drupal module's
        settings.




** MODULE CONFIGURATION **

Configure Mycelium Gear options in admin/commerce/config/payment-methods
by editing the Mycelium Gear payment method. The configuration
options are part of the reaction rule settings.

    - Gateway Secret

        This is provided by Mycelium the first time you create your gateway.
        You can create your gateway at https://admin.gear.mycelium.com/gateways
        The gateway secret should look something like this:
        15wvfMo3934S5XfQjBJs1qxSEneH7WLd1rTikLs45TiZhGHo1yuk4YW2hWRNHc8Q

    - Gateway ID

        The gateway id is provided by Mycelium and can be found in your Mycelium
        account in the gateways section.

    - Payment display

        Choose how the payment method is displayed to the customer while they
        are checking out.

    - Checkout redirect mode

        Choose the payment page. You can keep your customers on your site and
        use the built-in payment page or you can redirect your customers to a
        payment page hosted by Mycelium which is much more robust and is
        recommended.


** CONTACT **

Current maintainers:

* Wesley Jones (wesoccer2003)
  https://www.drupal.org/u/wesoccer2003
