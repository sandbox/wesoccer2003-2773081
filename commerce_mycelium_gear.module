<?php

/**
 * @file
 * Provides a Mycelium payment method for Drupal Commerce.
 */

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_mycelium_gear_commerce_payment_method_info() {
  $payment_methods['mycelium_gear'] = array(
    'base' => 'commerce_mycelium_gear',
    'title' => t('Mycelium Gear - Bitcoin'),
    'short_title' => t('Mycelium Gear'),
    'display_title' => t('Bitcoin'),
    'description' => t('Mycelium Gear'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => FALSE,
  );

  return $payment_methods;
}

/**
 * Implements hook_menu().
 */
function commerce_mycelium_gear_menu() {
  // Add a menu item for capturing callbacks from Mycelium.
  $items['mycelium/callback'] = array(
    'title' => 'Mycelium Callback',
    'page callback' => 'commerce_mycelium_gear_process_callback',
    'access callback' => 'commerce_mycelium_gear_callback_access',
    'type' => MENU_CALLBACK,
    'file' => 'includes/commerce_mycelium_gear.ipn_callback.inc',
  );
  // The URL that you should configure in your Mycelium Gateway
  // for the "After payment redirect to".
  $items['mycelium/checkout/complete'] = array(
    'title' => 'Complete checkout',
    'page callback' => 'commerce_mycelium_gear_complete_payment',
    'access callback' => 'commerce_mycelium_gear_complete_checkout_access',
    'type' => MENU_CALLBACK,
    'file' => 'includes/commerce_mycelium_gear.checkout_callback.inc',
  );
  return $items;
}

/**
 * Menu access callback for Mycelium callbacks.
 *
 * Reference:
 * https://admin.gear.mycelium.com/docs/api/receiving_order_status_change_callback.
 *
 * @return bool
 *   TRUE if callback signature is valid, FALSE otherwise.
 */
function commerce_mycelium_gear_callback_access() {
  $access = FALSE;

  if (isset($_SERVER['HTTP_X_SIGNATURE'])) {
    $query = drupal_get_query_parameters();
    $signature = $_SERVER['HTTP_X_SIGNATURE'];
    $transaction = commerce_payment_transaction_load($query['callback_data']);
    $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
    $empty_hash = hash('sha512', '', TRUE);
    $request = 'GET' . request_uri() . $empty_hash;
    $generated_signature = hash_hmac('sha512', $request, $payment_method['settings']['gateway_secret'], TRUE);
    $encoded = base64_encode($generated_signature);
    $access = $signature === $encoded;
  }
  return $access;
}

/**
 * Menu access to the complete checkout page.
 *
 * This occurs when you are returned back to your site from Mycelium.
 *
 * @return bool
 *   TRUE if callback signature is valid, FALSE otherwise.
 */
function commerce_mycelium_gear_complete_checkout_access() {
  $access = FALSE;
  if (_commerce_mycelium_gear_lists_session("order_id")
    && _commerce_mycelium_gear_lists_session('transaction_id')) {
    $access = TRUE;
  }
  return $access;
}

/**
 * Load an existing transaction to update the status.
 *
 * @param object $order
 *   Commerce order.
 * @param object $transaction
 *   Commerce transaction.
 * @param array $ipn
 *   Query string transaction information sourced from Mycelium.
 */
function commerce_mycelium_gear_update_transaction($order, $transaction, array $ipn) {
  switch ($ipn['status']) {

    // Unconfirmed; transaction was received, but does not have enough
    // confirmations yet.
    case '1':
      commerce_checkout_complete($order);
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t('Paid, waiting for more blockchain confirmations.');
      break;

    // Paid in full.
    case '2':
      commerce_checkout_complete($order);
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('Payment confirmed.');
      break;

    // Underpaid; not enough money received.
    case '3':
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t("Underpaid.");
      break;

    // Overpaid; too much has been received.
    case '4':
      commerce_checkout_complete($order);
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t("Overpaid.");
      break;

    case '5':
      // Expired invoices don't generate an automatic IPN reply.
      // Expired invoices must be queried.
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('Payment was not received within 15 minutes from invoice generation.');
      break;

    case '6':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('Customer canceled the order.');
      break;

    default:
      watchdog('commerce_mycelium_gear', 'Unknown payment status for transaction @id: @status', array('@id' => $transaction->transaction_id, '@status' => $ipn['status']), WATCHDOG_WARNING);
      return;
  }
  $transaction->remote_status = $ipn['status'];
  $transaction->payload = $ipn;

  commerce_payment_transaction_save($transaction);
}

/**
 * Returns the default settings for the Mycelium payment method.
 *
 * @return array
 *   Returns the default settings to populate the configuration form.
 */
function commerce_mycelium_gear_default_settings() {

  return array(
    'gateway_secret' => '',
    'gateway_id' => '',
    // Show text, icon, or both on payment choice page.
    'checkout_display' => 'both',
    // Options: offsite = mycelium hosted checkout;
    // onsite = never leave your site.
    'redirect_mode' => 'offsite',
  );
}

/**
 * Payment method callback: settings form.
 */
function commerce_mycelium_gear_settings_form($settings = array()) {
  $form = array();

  // Merge default settings into the stored settings array.
  $settings = (array) $settings + commerce_mycelium_gear_default_settings();

  $form['gateway_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Gateway Secret'),
    '#default_value' => $settings['gateway_secret'],
    '#description' => t("This is only provided once by Mycelium when you first setup your gateway."),
  );
  $form['gateway_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Gateway ID'),
    '#default_value' => $settings['gateway_id'],
    '#description' => t("This can be found in your Mycelium account's gateway settings."),
  );
  $form['checkout_display'] = array(
    '#type' => 'radios',
    '#title' => t('Payment display'),
    '#options' => array(
      'text' => t('Bitcoin text only'),
      'icon' => t('Bitcoin icon only'),
      'both' => t('Both text and icon'),
    ),
    '#default_value' => $settings['checkout_display'],
    '#description' => t('When selecting a payment option, select the indicator next to the radio buttons for payment options.'),
  );
  $form['redirect_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Checkout redirect mode'),
    '#options' => array(
      'onsite' => t('Stay on this site displaying the payment details on the checkout page'),
      'offsite' => t('Redirect to a checkout page hosted by Mycelium (Recommended)'),
    ),
    '#description' => t('Using the redirect option will require a different configuration in your Mycelium account. Please read the Commerce Mycelium module documentation for reference.'),
    '#default_value' => $settings['redirect_mode'],
  );

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_mycelium_gear_form_commerce_checkout_form_alter(&$form, &$form_state) {
  // If this checkout form contains the payment method radios.
  if (!empty($form['commerce_payment']['payment_method']['#options'])) {
    // Loop over its options array looking for a Bitcoin option.
    foreach ($form['commerce_payment']['payment_method']['#options'] as $key => &$value) {
      $method_id = explode('|', $key);

      switch ($method_id[0]) {
        case 'mycelium_gear':
          // Prepare the replacement radio button text with icons.
          $variables = array(
            'path' => drupal_get_path('module', 'commerce_mycelium_gear') . '/images/bitcoin.png',
            'title' => t('Bitcoin accepted by Mycelium'),
            'alt' => t('Bitcoin logo'),
            'attributes' => array(
              'class' => array('commerce-mycelium-icon'),
            ),
          );
          $icons['bitcoin'] = theme('image', $variables);

          $payment_method = commerce_payment_method_instance_load($key);

          if (isset($payment_method['settings']['checkout_display'])) {
            $value = '';
            if (in_array($payment_method['settings']['checkout_display'], array('both', 'text'))) {
              $value .= t('Bitcoin');
            }
            if (in_array($payment_method['settings']['checkout_display'], array('both', 'icon'))) {
              $value .= '<span class="commerce-mycelium-icons">' . implode(' ', $icons) . '</span>';
            }
          }

          // Add the CSS to place the icon after the radio button text.
          $form['commerce_payment']['payment_method']['#attached']['css'][] = drupal_get_path('module', 'commerce_mycelium_gear') . '/theme/commerce_mycelium_gear.theme.css';

          break;
      }
    }
  }
}

/**
 * Implements hook_commerce_checkout_page_info_alter().
 */
function commerce_mycelium_gear_commerce_checkout_page_info_alter(&$checkout_pages) {
  // @TODO: Defect - this message shows up for other payment methods as well. It
  // should be restricted to only be shown when this payment method is
  // the selected one.
  $checkout_pages['payment']['help'] = t('Use your Bitcoin client to send payment to this address. Once the payment is acknowledged on the blockchain, it will automatically complete your checkout.');
}

/**
 * Creates a payment transaction for the specified charge amount.
 *
 * @param array $payment_method
 *   The payment method instance object used to charge this payment.
 * @param object $order
 *   The order object the payment applies to.
 * @param array $charge
 *   An array indicating the amount and currency code to charge.
 */
function commerce_mycelium_gear_transaction(array $payment_method, $order, array $charge) {
  $transaction = commerce_payment_transaction_new('mycelium_gear', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->remote_id = '';
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
  $transaction->message = 'Initialized transaction';
  $transaction->message_variables = array();

  commerce_payment_transaction_save($transaction);
}

/**
 * Payment method callback: submit form submission.
 *
 * Processes payment as necessary using data inputted via the payment details
 * form elements on the form, resulting in the creation of a payment
 * transaction.
 *
 * @param array $payment_method
 *   An array containing payment_method_info hook values and user settings.
 */
function commerce_mycelium_gear_submit_form_submit(array $payment_method, $pane_form, $pane_values, $order, $charge) {
  // Display an error and prevent the payment attempt if the payment
  // method has not been configured yet.
  if (empty($payment_method['settings']['gateway_id']) || empty($payment_method['settings']['gateway_secret'])) {
    drupal_set_message(t('The @payment_method payment method must be configured by an administrator before it can be used.',
      array('@payment_method' => $payment_method['display_title'])), 'error');
    return FALSE;
  }

  // Get order to wrapper.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get order price.
  $amount = $wrapper->commerce_order_total->amount->value();

  if (empty($amount)) {
    watchdog('commerce_mycelium_gear', 'Skipping payment on order @id for zero balance.', array('@id' => $order->order_id), WATCHDOG_INFO, l(t('view order'), 'admin/commerce/orders/' . $order->order_id));
    commerce_checkout_complete($order);
    drupal_goto(url('checkout/' . $order->order_id . '/complete'));
  }

  $order->data['mycelium_gear'] = $pane_values;

  commerce_mycelium_gear_transaction($payment_method, $order, $charge);
}

/**
 * Payment method callback: redirect form.
 *
 * For the hosted checkout page, this form automatically redirects to the
 * Mycelium hosted invoice page through an HTTP GET request. For the
 * onsite option, this returns form values for displaying markup elements
 * necessary to embed the payment details.
 *
 * @param array $form
 *   Probably an empty array when this gets executed.
 * @param array $form_state
 *   Form submission data including order node information and payment method
 *   information.
 * @param object $order
 *   An object of general order information.
 * @param array $payment_method
 *   An array containing payment_method_info hook values and user settings.
 */
function commerce_mycelium_gear_redirect_form(array $form, array &$form_state, $order, array $payment_method) {

  // Get order to wrapper.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get order price.
  $amount = $wrapper->commerce_order_total->amount->value();
  $order_total = $wrapper->commerce_order_total->value();
  $decimal_amount = commerce_currency_amount_to_decimal($amount, $order_total['currency_code']);

  // Get transaction.
  $transaction_info = db_select('commerce_payment_transaction', 'cpt')
    ->fields('cpt', array('transaction_id', 'remote_id', 'message', 'changed'))
    ->condition('cpt.payment_method', 'mycelium_gear')
    ->condition('cpt.order_id', $order->order_id)
    ->range(0, 1)
    ->addTag('commerce_mycelium_gear_redirect_transaction')
    ->execute()
    ->fetchAssoc();
  if (!$transaction_info || !is_array($transaction_info)) {
    watchdog('commerce_mycelium_gear', 'Failed to load a transaction for order @id.', array('@id' => $order->order_id));
    return FALSE;
  }

  $uri = '/gateways/' . $payment_method['settings']['gateway_id'] . '/orders';

  // Get the address_index and increment it.
  $address_index = db_query("SELECT address_index FROM {commerce_mycelium_gear}")->fetchField();
  db_update('commerce_mycelium_gear')
    ->expression('address_index', 'address_index + 1')
    ->execute();

  /*
   * Documentation on creating the signature.
   * https://admin.gear.mycelium.com/docs/api/signed_request
   */
  $microtime = explode(' ', microtime());
  $nonce = $microtime[1] . substr($microtime[0], 2, 6);
  $body = '{"amount":' . $decimal_amount . ',"keychain_id":' . $address_index . ',"callback_data":"' . $transaction_info['transaction_id'] . '"}';
  $nonce_body_hash = hash('sha512', $nonce . $body);
  $request = 'POST' . $uri . $nonce_body_hash;
  $signature = hash_hmac('sha512', $request, $payment_method['settings']['gateway_secret']);

  // Create the POST to send to Mycelium.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'X-Nonce: ' . $nonce,
    'X-Signature: ' . $signature,
  ));
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
  curl_setopt($ch, CURLOPT_URL, 'https://gateway.gear.mycelium.com' . $uri);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  $result = curl_exec($ch);

  // Log any errors to the watchdog.
  if ($error = curl_error($ch)) {
    watchdog('commerce_mycelium_gear', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    return FALSE;
  }

  curl_close($ch);
  $response = json_decode($result);

  if ($response) {

    switch ($payment_method['settings']['redirect_mode']) {
      case 'offsite':
        // Save these variables so you have them when you get
        // redirected back from the mycelium payment page.
        _commerce_mycelium_gear_lists_session("order_id", $order->order_id);
        _commerce_mycelium_gear_lists_session("transaction_id", $transaction_info['transaction_id']);

        // Redirect to Mycelium hosted invoice page.
        drupal_goto('https://gateway.gear.mycelium.com/pay/' . $response->payment_id);
        break;

      case 'onsite':
        // Front end code to display a Bitcoin invoice.
        $mycelium_path = drupal_get_path('module', 'commerce_mycelium_gear');
        drupal_add_js($mycelium_path . '/commerce_mycelium_gear_redirect.js', array('cache' => FALSE));

        drupal_add_js(array('myceliumGatewayId' => $payment_method['settings']['gateway_id']), 'setting');
        drupal_add_js(array('myceliumOrderId' => $response->payment_id), 'setting');
        drupal_add_js(array('drupalOrderId' => $order->order_id), 'setting');

        $form['mycelium_information'] = array(
          '#markup' => '<img src="https://chart.googleapis.com/chart?chs=165x165&cht=qr&chl=bitcoin:'
          . $response->address . '?amount=' . $response->amount_in_btc . '"><br />'
          . t('<strong>Amount: </strong>@amount BTC', array('@amount' => $response->amount_in_btc)) . '<br />'
          . t('<strong>Address: </strong>@address', array('@address' => $response->address)) . '<br />'
          . t('<strong><span id="timerLabel">Time left until the order expires:</span></strong>')
          . ' <span id="timer"></span><br />'
          . t('<strong>Status: </strong><span id="status">Listening to the blockchain...</span>') . '<br /><br />',
        );
        return $form;
    }
  }
  else {
    watchdog('commerce_mycelium_gear', 'mycelium error: @error', array('@error' => $result), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Helper method that stores session variables.
 *
 * Reference:
 * http://stackoverflow.com/questions/10490253/how-to-save-a-session-variable-in-drupal-7.
 */
function _commerce_mycelium_gear_lists_session($key, $value = NULL) {
  global $user;

  static $storage;

  // If the user is anonymous, force a session start.
  if ($user->uid) {
    drupal_session_start();
  }

  if ($value) {
    $storage[$key] = $value;
    // Uses 'lists' in case some other module uses 'type' in $_SESSION.
    $_SESSION['lists'][$key] = $value;
  }
  elseif (empty($storage[$key]) && isset($_SESSION['lists'][$key])) {
    $storage[$key] = $_SESSION['lists'][$key];
  }
  return $storage[$key];
}

/**
 * Implements hook_help().
 */
function commerce_mycelium_gear_help($path, $arg) {
  switch ($path) {
    case 'admin/help#commerce_mycelium_gear':

      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}
