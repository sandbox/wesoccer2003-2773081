/**
 * @file
 * Provides client-side support when checking out with Mycelium Gear.
 *
 * This Javascript is loaded on the onsite payment page to
 * automatically detect when a transaction has occurred and
 * to redirect them to the complete checkout page.
 */

// Check for page to be loaded.
document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var fifteenMinutes = 60 * 15;
  var display = document.querySelector('#timer');
  var countdown = startTimer(fifteenMinutes, display);

  var myceliumSocket = new WebSocket('wss://gateway.gear.mycelium.com/gateways/' +
    Drupal.settings.myceliumGatewayId + '/orders/' +
  Drupal.settings.myceliumOrderId + '/websocket');

  myceliumSocket.onmessage = function (event) {
    var msg = JSON.parse(event.data);
    var log = document.getElementById('status').innerHTML + '<br />';

    switch (msg.status) {

      // Unconfirmed.
      case 1:
        document.getElementById('status').innerHTML = log +
          'Transaction was received! Waiting on more confirmations...';
        break;

      // Paid in full.
      case 2:
        document.getElementById('status').innerHTML = log + 'Paid.';
        clearInterval(countdown);
        document.getElementById('timerLabel').innerHTML = 'Continuing in: ';
        // Wait 5 seconds for the IPN callback to complete your order.
        startTimer(5, display);
        setTimeout(function () {
          window.location = '/checkout/' + Drupal.settings.drupalOrderId + '/complete';
        }, 5000);
        break;

      // Underpaid.
      case 3:
        document.getElementById('status').innerHTML = log + 'Not enough money received. You still owe ' +
          msg.amount_to_pay_in_btc + 'BTC';
        break;

      // Overpaid.
      case 4:
        document.getElementById('status').innerHTML = log + "You've sent too much. You will have to contact " +
          'the site administrator if you need to be refunded. ' + msg.amount_paid_in_btc + ' BTC was received. ' +
          msg.amount_in_btc + ' BTC was requested.';
        break;

      // Expired.
      case 5:
        document.getElementById('status').innerHTML = log +
          'Time has expired. Please place a new order.';
        break;

      // Canceled.
      case 6:
        document.getElementById('status').innerHTML = log + 'Your order has been canceled.';
        break;

      default:
        document.getElementById('status').innerHTML = log +
          'An issue occurred. Please wait a few more minutes to see if the payment goes through. Otherwise contact the site administrator';
    }
  };
});

function startTimer(duration, display) {
  'use strict';
  var timer = duration;
  var minutes;
  var seconds;
  var countdown = setInterval(function () {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    display.textContent = minutes + ':' + seconds;

    if (--timer < 0) {
      window.location = '/cart';
    }
  }, 1000);
  return countdown;
}
