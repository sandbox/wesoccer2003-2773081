<?php

/**
 * @file
 * Return URL so Drupal can complete the order.
 */

 /**
  * Completes the checkout when redirected back to the site from mycelium.
  */
function commerce_mycelium_gear_complete_payment() {
  $order = commerce_order_load(_commerce_mycelium_gear_lists_session("order_id"));
  $transaction = commerce_payment_transaction_load(_commerce_mycelium_gear_lists_session('transaction_id'));

  if ($transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS) {
    drupal_goto('/checkout/' . $order->order_id . '/complete');
  }
  // The IPN callback has not been received.
  else {
    watchdog('commerce_mycelium_gear', 'The IPN callback was not received before the user was returned to your site for order: @order', array('@order' => $order->order_id), WATCHDOG_ERROR);
    drupal_set_message(t('Please contact the site administrator. An issue has occurred with your order.'), 'error');
    drupal_goto('/');
  }
}
