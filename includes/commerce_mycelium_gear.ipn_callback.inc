<?php

/**
 * @file
 * Transaction callbacks from Mycelium Gear.
 */

/**
 * Interpret IPN updates from Mycelium and update relevant payment transactions.
 */
function commerce_mycelium_gear_process_callback() {
  $query = drupal_get_query_parameters();
  if ($query['callback_data']) {
    $transaction = commerce_payment_transaction_load($query['callback_data']);
    $order = commerce_order_load($transaction->order_id);

    watchdog('commerce_mycelium_gear', 'IPN received for order @order_id, transaction @trans_id.', array('@trans_id' => $transaction->transaction_id, '@order_id' => $order->order_id), WATCHDOG_DEBUG, l(t('view order payments'), 'admin/commerce/orders/' . $order->order_id . '/payment'));

    if ($order->status !== 'pending'
      && $order->status !== 'completed'
      && $order->status !== 'checkout_complete') {
      commerce_mycelium_gear_update_transaction($order, $transaction, $query);
    }
  }
  else {
    watchdog('commerce_mycelium_gear', 'The Mycelium IPN callback did not contain the callback_data param for keychain_id: @keychain_id', array('@keychain_id' => $query['keychain_id']), WATCHDOG_ERROR);
  }
}
